import html_analyze

html = open('test.html', 'r').read()

print('Testing get_by_attributes() without external links:')

got = html_analyze.get_by_attributes(html, 'https://www.google.com/')

print(str(got)+'\n')

print('Testing get_by_attributes() with external links:')

got = html_analyze.get_by_attributes(html, 'https://www.google.com/', True)

print(got)