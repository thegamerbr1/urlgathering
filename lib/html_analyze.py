import uritools

from validators import url as urltest
from bs4 import BeautifulSoup

def get_by_attributes(html, baseurl, extern=False):
    urlist = []
    soup = BeautifulSoup(html, 'html.parser') #Create a BeautifulSoup parser object, with the html passed as argument to the function
    baseurl_composed = uritools.urisplit(baseurl) #Split the baseurl
    
    for tag in soup.find_all(attrs={'href': True}) + soup.find_all(attrs={'src': True}): #For each tag that has the href or src attributes
        if tag.attrs.get('href'): #Verify if the tag has the src or href attribute, and defines "url" as the content of the attribute.
            url = tag['href']
        else:
            url = tag['src']
        
        if urltest(url) and url not in urlist: #If the url it self is a valid URL and isn't in the urlist, append it to the list.
            if not extern and uritools.urisplit(url).authority != baseurl_composed.authority:
                continue
                
            urlist.append(url)
        elif url.startswith('/'): #If the url starts with a "/", meaning it is a path, add it to the end of the baseurl, and append it to the list.
            new_url = uritools.urijoin(baseurl, url)
            if not urltest(new_url):
                continue
                
            elif new_url not in urlist:
                urlist.append(new_url)
        elif url.startswith('//'): #If the url starts with a "//", meaning it is a url without the scheme (http/https), with  appends it to the list.
            new_url = baseurl_composed.scheme + ':' + url
            
            if urltest(new_url):
                urlist.append(new_url)
        else:
            continue
    
    return urlist

def analyze(html, baseurl):
    urlist = [] #List of found URLs
    
    urlist = urlist + get_all_urls(html, baseurl)