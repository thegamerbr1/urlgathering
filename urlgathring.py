import argparse

__version__ = '0.1.1' #Version of urlgathering

def max_recursion(n):
    n = int(n)
    if n > 100 or n < 1:
        raise Exception('WTF')
    return n

class verbose: #Class that controls the verbose messaging
    def __init__(self, verbose):
        self.verbose = verbose
    def print(self, *args, **kwargs):
        if self.verbose:
            print(*args, **kwargs)

parser = argparse.ArgumentParser(prog='urlgathering',
                                  description='urlgathering it\'s a simple URL gatherer, by requesting a GET HTTP method and recursivly listing the found URLs in the HTML.') #Initialize the command-line argument parser

protocolgroup = parser.add_mutually_exclusive_group() #Add a parser group that only one parameter can be permited.

parser.add_argument('main_url', metavar='main_url', action='store', help='the point of start url to search for other urls') #Add the main_url argument to the parser
parser.add_argument('-v', '--version', action='version', version='(prog) {version}'.format(version=__version__), help='show the version of the program') #Add the --version parameter to the parser
parser.add_argument('--verbose', action='store_true', help='enable verbose mode')
parser.add_argument('-m', '--max-page-recursion', metavar='max_value', action='store', type=max_recursion, help='the maximum page recursion of url searching (default: 10 max: 100)')
parser.add_argument('-o', '--output', metavar='filename', action='store', type=str, help='do not shows in the screen the found urls, and outputs it to the specified file')

protocolgroup.add_argument('--http', action='store_true', help='force the use of HTTP') #Add the --http parameter to the parser
protocolgroup.add_argument('--https', action='store_true', help='force the use of HTTPS') #Add the --https parameter to the parser

args = parser.parse_args() #Parse the arguments (from sys.argv)

print('Starting urlgathering {version}...\n'.format(version=__version__))

if args.http: #Verify the protocol to be used
    print('User selected http protocol...')
    prot = 'http'
elif args.https:
    print('User selected https protocol...')
    prot = 'https'
else:
    print('Assuming https protocol...')
    prot = 'https'

#TODO: everything.